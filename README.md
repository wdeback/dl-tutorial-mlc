Description
-----------

Installation
------------

- Download and install miniconda for python 3.x from

    https://conda.io/miniconda.html

- Clone this git repository

```bash
git clone git@gitlab.com:wdeback/dl-tutorial-mlc.git
```

or without gitlab account

```bash
git clone https://gitlab.com/wdeback/dl-tutorial-mlc.git
```

- Create python 3.5 environment with required packages (keras, scikit-learn, tensorflow, etc.)

```bash
cd dl-tutorial-mlc
conda env create --file environment.yml
```

This will install all dependencies within a virtual environment called 'dl'.

Start
-----

- Activate environment

```bash
source activate dl
```

- Start notebook

```bash
jupyter lab
```

- Open a notebook in folder `notebooks`

- Execute cell with `Shift+Enter`
  - [Quick start with jupyter notebook](http://cs231n.github.io/ipython-tutorial/)
  - [Notebook 
basics](http://nbviewer.jupyter.org/github/jupyter/notebook/blob/master/docs/source/examples/Notebook/Notebook%20Basics.ipynb#Overview-of-the-Notebook-UI)

Recommended resources
---------------------

- [CS231n Stanford course](http://cs231n.stanford.edu/) on Convolutional Neural Networks for Visual Recognition
- F. Chollet's book on [Deep Learning with Python](https://www.manning.com/books/deep-learning-with-python) and the 
[associated notebooks](https://github.com/fchollet/deep-learning-with-python-notebooks)
- [Cheat sheets](https://gitlab.com/wdeback/cheat-sheets) for data science and deep learning in python

Author
------

Walter de Back

Institute for Medical Informatics and Biometry (IMB)

Faculty of Medicine

TU Dresden, Germany

gitlab: @wdeback
  
twitter: [@wdeback](http://twitter.com/wdeback)

homepage: [walter.deback.net](http://walter.deback.net)

